// Copyright 2016 Philipp Matthias Schaefer <philipp.matthias.schaefer@posteo.de>
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Execute a block after computing a result.
//!
//! This crate provides the macro `prog1`, an implementation of the homonymous
//! Common Lisp macro.
//!
//! It allows to compute a result, before executing a block that changes values
//! previously used to compute the result.
//!
//! # Examples
//!
//! ```
//! #[macro_use]
//! extern crate prog1;
//!
//! fn main() {
//!     let mut a = 0;
//!
//!     assert_eq!(prog1!(a; { a += 1 }), 0);
//!     assert_eq!(a, 1);
//! }
//! ```

/// Executes $after after computing the result $result.
///
/// For more information see the crate level documentation.
#[macro_export]
macro_rules! prog1 {
    ( $result:expr; $after:block) => {{
        let result = $result;
        $after
        result
    }}
}
